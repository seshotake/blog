/** @jsx h */

import blog, { redirects } from "blog";

blog({
  title: "seshotake",
  description: "Something...",
  avatar: "https://avatars.githubusercontent.com/u/116971836?v=4",
  avatarClass: "rounded-full",
  author: "seshotake",
  middlewares: [
    redirects({
      "/docker-arch": "installing-docker-in-arch",
    }),
  ],
  links: [
    { title: "Email", url: "mailto:seshotake@gmail.com" },
    { title: "Github", url: "https://github.com/seshotake" },
  ],
});
