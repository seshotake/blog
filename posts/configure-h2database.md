---
title: Configure Spring Boot With H2 Database
publish_date: 2022-11-03
tags: ["java", "spring", "h2database"]
---

- [Step 1: Set dependencies in `pom.xml`](#step-1-set-dependencies-in-pomxml)

## Step 1: Set dependencies in `pom.xml`

```xml
<dependency>
  <groupId>org.springframework.boot</groupId>
  <artifactId>spring-boot-starter-data-jpa</artifactId>
</dependency>
<dependency>
  <groupId>com.h2database</groupId>
  <artifactId>h2</artifactId>
  <scope>runtime</scope>
</dependency>
```

## Step 2: Configurate database

Add the following properties to the `application.properties` file:

```
spring.datasource.url=jbc:h2:mem:testdb
spring.datasource.username=""
spring.datasource.password=""
```

> note: `spring.datasource.url=jbc:h2:mem:testdb` - configures the application to connect to an [in-memory](/posts/h2database.md#in-memory-database) store
