---
title: Installing Docker in Arch Linux
publish_date: 2022-06-02
tags: ["archlinux", "docker"]
---

- [Step 1: Update Arch Linux](#step-1-update-arch-linux)

- [Step 2: Installing Docker](#step-2-installing-docker)

- [Step 3: Starting the docker service on startup](#step-3-starting-the-docker-service-on-startup)

- [Step 4: Adding User to Docker group](#step-4-adding-user-to-docker-group)

## Step 1: Update Arch Linux

```bash
$ sudo pacman -Syu
```

## Step 2: Installing Docker

```bash
$ sudo pacman -S docker
```

## Step 3: Starting the docker service on startup

```bash
$ sudo systemctl enable docker.service
```

## Step 4: Adding User to Docker group

```bash
$ sudo usermod -aG docker $USER
```
