---
title: H2 Database
publish_date: 2022-11-03
tags: ["hava", "h2database"]
---

- [Connection modes](#connection-modes)
  - [embedded mode](#embedded-mode)
  - [server mode](#server-mode)
  - [mixed mode: combination of embedded and server modes](#mixed-mode-combination-of-embedded-and-server-modes)
- [In-Memory Database](#in-memory-database)

## Connection modes

We configure to this modes:

### embedded mode

> (url: `jdbc:h2:[file:][<path>]<databaseName>`)

- the fastest and easiest connection mode

- may only be open in one VM

- recommended to use the client-server model instead

### server mode

> (url: `jdbs:h2:[ssl|tcp]://<server>[:<port>]/[<path>]<databaseName>`)

- application opens a database remotely using the JDBC or ODBC API

- many applications can connect to the same database at the same time, by connecting to this server.

- slower than the embedded mode

### mixed mode: combination of embedded and server modes

## In-Memory Database

The in-memory database is volatile, and results in data loss after application restart.
