---
title: Helix Cheatsheet
publish_date: 2022-08-02
tags: ["programming", "helix"]
---

## Movement

### `CURSOR MOVEMENT`

- `h`, `Left` — move left
- `j`, `Down` — move down
- `k`, `Up` — move up
- `l`, `Right` — move right

### `QUICK MOVEMENT`

- `gl`, `End` — go to the end of the line
- `gh`, `Home` — go to the start of the line
- `gs` — go to the first non-blank character of the line
- `gg` — go to the start of the file
- `ge` — go to the end of the file
- `5gg` or `5G` — go to the line 5

### `CURSOR MOVEMENT TO SCREEN`

- `gt` — go to the top of the screen
- `gc` — go to the middle of the screen
- `gb` — go to the bottom of the screen

### `CONTENT MOVEMENT`

- `w` — move next word start
- `b` — move previous word start
- `e` — move next word end
- `W` — move next WORD start
- `B` — move previous WORD start
- `E` — move next WORD end

### `OCURRENCE MOVEMENT`

- `te` — find ‘till next char `e`
- `fe` — find next char `e`
- `Te` — find ‘till previous char `e`
- `Fe` — find previous char `e`

### `REPEAT MOVEMENT`

- `Alt-.` — repeat last motion (`f`, `t` or `m`)

### `SCREEN MOVEMENT`

- `Ctrl-b`, `PageUp` — move page up

- `Ctrl-f`, `PageDown` — move page down

- `Ctr-u` — move half page up

- `Ctrl-d` — move half page down

### `JUMPING MOVEMENT`

- `Ctrl-s` — save the current selection to the jumplist

- `Ctrl-o` — jump backward on the jumplist

- `Ctrl-i` — jump forward on the jumplist

- `Space-j` — open jumplist picker

### `BUFFER MOVEMENT`

- `gn` - go to next buffer
- `gb` - go to previous buffer

## Insert mode

### `BASIC INSERTION`

- `i` — insert before selection
- `a` — insert after selection
- `I` — insert at the start of the line
- `A` — insert at the end of the line

### `NEWLINE INSERTION`

- `Ctrl-j`, `Enter` - insert new line

### `APPEND`

- `o` — open new line below selection
- `O` — open new line above selection

### `DELETION`

- `Backspace`, `Ctrl-h` - delete previous char
- `Delete`, `Ctrl-d` - delete next char
- `Ctrl-w`, `Alt-Backspace` - delete previous word
- `Alt-d`, `Alt-Delete` - delete next word
- `Ctrl-u` - delete to start of line
- `Ctrl-k` - delete to end of line

### `EXIT INSERT MODE`

- `Esc` - switch to normal mode

### `REPEAT`

- `.` - repeat last insertion

## Editing

### `REPLACE`

- `r` - replace with a character
- `R` - replace with yanked text
- `Space-R` - replace selections by clipboard contents

### `CASE SWITCHING`

- `~` - switch case of the selected text
- `` ` `` - set the selected text to lower case
- `` Alt-` `` - set the selected text to upper case

### `UNDO AND REDO`

- `u` - undo change
- `U` - redo change
- `Alt-u` - move backward in history
- `Alt-U` - move forward in history

### `CHANGE`

- `c` - change selection
- `Alt-c` - change selection without yanking
- `xc` - change entire line
- `vglc` or `t<ret>c` - change to the end of the line
- `vghc` - change to the begin of the line
- `miwc` - change entire word
- `wc` or `ec` - change to the end of the word
- `bc` - change to the begin of the word

### `COPYING`

- `y` - yank selection
- `Space-y` - join and yank selections to clipboard
- `Space-Y` - yank main selection to clipboard

### `PASTE`

- `p` - paste after selections
- `P` - paste before selections
- `Space-p` - paste system clipboard after selections
- `Space-P` - paste system clipboard before selections

### `DELETION`

- `d` - delete selection
- `Alt-d` - delete selection without yanking
- `;d` - delete character
- `xd` - delete entire line
- `vgld` or `t<ret>d` - delete to the end of the line
- `vghd` - delete to the begin of the line
- `miwd` - delete entire word
- `dc` or `dc` - delete to the end of the word
- `bc` - delete to the begin of the word

### `JOIN`

- `J` - join lines inside selection

### `COMMENT`

- `Ctr-c` - comment/uncomment the selections

### `INCREMENT/DECREMENT NUMBER`

- `Ctrl-a` - increment object under cursor
- `Ctrl-x` - decrement object under cursor

## Select Mode

### `BASIC SELECT MODE`

- `v` - start select mode
- `%` - select entire file
- `x` - select current line, if already selected, extend to next line
- `X` - extend selection to line bounds

### `REGEX SELECTION`

- `s` - select all regex matches inside selections
- `S` - split selection into subselections on regex matches

### `MULTILINE SELECTION`

- `C` - copy selection onto the next line (add cursor below)
- `Alt-C` - copy selection onto the previous line (add cursor below)
- `,` - keep only the primary selection (works only with multiline selections)
- `Alt-,` - remove the primary selection

### `ALIGN AND TRIM`

- `&` - align selection in columns (works only with multiline selections)
- `_` - trim whitespace from the selection

### `COLLAPSE SELECTION`

- `;` - collapse selection onto a single cursor
- `Alt-;` - flip selection cursor and anchor

## PICKER

### `ENTRY MOVEMENT`

- `Down`, `Ctrl-n` - next entry
- `Up`, `Ctrl-p` - previous entry

### `QUICK MOVEMENT`

- `PageUp`, `Ctrl-u` - page up
- `PageDown`, `Ctrl-d` - page down
- `Home` - go to first entry
- `Down` - go to last entry

### `OPEN ENTRY`

- `Enter` - open selected
- `Ctrl-s` - open horizontally
- `Ctrl-v` - open vertically

### `TOGGLE PREVIEW`

- `Ctrl-t` - toggle preview

### `EXIT PICKER`

- `Esc`, `Ctrl-c` - close picker

### `PICKERS`

- `Space-f` - open file picker
- `Space-b` - open buffer picker
- `Space-j` - open jumplist picker
- `'` - open last fuzzy picker

## LSP

### `BASICS`

- `=` - format selection (currently disabled)
- `Space-k` — show documentation for item under cursor in a popup
- `Space-r` — rename symbol
- `Space-a` — apply code action
- `Space-s` — open document symbol picker
- `Space-S` — open workspace symbol picker

### `GO TO`

- `gd` — go to definition
- `gy` — go to type definition
- `gr` — go to references
- `gi` — go to implementation

### `DIAGNOSTICS`

- `[d` — go to previous diagnostic
- `]d` — go to next diagnostic
- `[D` — go to first diagnostic in document
- `]D` — go to last diagnostic in document
- `Space-d` — open document diagnostics picker
- `Space-D` — open workspace diagnostics picker
